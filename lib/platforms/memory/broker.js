const _ = require("lodash");

const Task = require("../../task");
const Broker = require("../../broker");

class MemoryBroker extends Broker {

    constructor() {
        super();
        this.setMaxListeners(Infinity);

        /** @member {Map<String, SerializedTask[]>} */
        this.queues = new Map();

        setImmediate(() => this.emit("ready"));
    }

    /**
     * @override
     *
     * @param {Task} task
     * @param {Number} [delay]
     * @return {Promise<Task>}
     */
    enqueue(task, delay) {
        return new Promise((resolve) => {
            if (_.isPositiveNumber(delay)) {
                setTimeout(() => {
                    this.getQueue(task.name).push(task.serialize());
                }, delay);
            } else {
                this.getQueue(task.name)
                    .push(task.serialize());
            }

            this.emit(`${task.name}:new`);

            resolve(task);
        });
    }

    /**
     * @override
     *
     * @param {String} name
     * @return {Promise<Task>}
     */
    dequeue(name) {
        return new Promise((resolve) => {
            resolve(this.getQueue(name).shift());
        });
    }

    /**
     * @override
     *
     * @param {String} name
     * @return {Promise<Number>}
     */
    tasksCount(name) {
        return Promise.resolve(this.getQueue(name).length);
    }

    /**
     * @override
     *
     * @param {String} name
     * @param {String} id
     * @return {Promise<Task>}
     */
    findTask(name, id) {
        const serialized = this.getQueue(name).find(t => t.id === id);

        if (serialized === undefined) return Promise.resolve(null);

        return Promise.resolve(Task.unserialize(serialized));
    }

    /**
     * @override
     *
     * @param {String} status
     * @param {Task} task
     * @param {*} result
     */
    ack(status, task, result) {
        this.emit(`${task.name}:${task.id}:${status}`, result);

        if (status === "failed" && task.opts.retries > 0) {
            task.opts.retries -= 1;

            const delay = Task.getDelay(task);

            this.enqueue(Task.unserialize(task), delay);
        }
    }

    /**
     * @private
     *
     * @param {String} name
     *
     * @return {SerializedTask[]}
     */
    getQueue(name) {
        if (!this.queues.has(name)) {
            this.queues.set(name, []);
        }

        return this.queues.get(name);
    }
}

module.exports = MemoryBroker;
