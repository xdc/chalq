const _ = require("lodash");
const { EventEmitter } = require("events");
const { v4 } = require("uuid");

/**
 * @typedef {Object} TaskOptions
 *
 * @param {Number} [retries]
 * @param {Number|Object} [delay]
 * @prop {String} [delay.backoff='linear'] Either "linear" or "exponential" or
 * "constant"
 * @prop {Number} [delay.amount=1000] The delay in milliseconds.
 */

/**
 * @typedef {Object} SerializedTask
 *
 * @property {String} id
 * @property {String} name
 * @property {Array<*>} args
 * @property {TaskOptions} opts
 */

/**
 * @typedef {Function} TaskHandler
 *
 * @param {...*} args
 *
 * @return {Promise<*>}
 */

/* eslint-disable no-param-reassign */
/**
 * Calculates delay for a task
 *
 * @return {Number} delay in milliseconds
 */
function calculateDelay(backoff, amount, retries) {
    amount = _.isPositiveNumber(amount) ? amount : 0;
    retries = _.isPositiveNumber(retries) ? retries : 1;

    switch (backoff) {
    case "linear":
        return amount * retries;
    case "exponential":
        return amount * Math.pow(2, retries - 1);
    default:
        return amount;
    }
}
/* eslint-enable no-param-reassign */

class Task extends EventEmitter {
    /**
     * @param {String} name
     * @param {Array<*>} args
     * @param {TaskOptions} opts
     */
    constructor(name, args, opts) {
        super();

        /** @member {String} */
        this.id = v4();

        /** @member {String} */
        this.name = name;

        /** @member {Array<*>} */
        this.args = args;

        /** @member {TaskOptions} */
        this.opts = _.pick(opts, ["retries", "delay", "timeout"]);

        if (_.isPlainObject(this.opts.delay)) {
            this.opts.delay = _.pick(this.opts.delay, ["backoff", "amount"]);
        }
    }

    /**
     * Serializes the task object into a plain object.
     *
     * @return {SerializedTask}
     */
    serialize() {
        return {
            id: this.id,
            name: this.name,
            args: this.args,
            opts: this.opts
        };
    }

    /**
     * Instantiates a new task from a serialized representation
     * @static
     *
     * @param {SerializedTask}
     *
     * @return {Task}
     */
    static unserialize(serialized) {
        const task = new Task(serialized.name, serialized.args, serialized.opts);

        task.id = serialized.id;

        return task;
    }

    /**
     * Retrieves the delay if the task is re-enqueued
     *
     * @param {Task} task
     *
     * @return {Number} delay in milliseconds
     */
    static getDelay(task) {
        if (_.isNumber(task.opts.delay)) {
            return task.opts.delay >= 0 ? task.opts.delay : 1000;
        }

        if (_.isPlainObject(task.opts.delay)) {
            const backoff = task.opts.delay.backoff || "linear";
            const amount = task.opts.delay.amount || 1000;

            return calculateDelay(backoff, amount, task.opts.retries);
        }

        return 0;
    }
}

module.exports = Task;
