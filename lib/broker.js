const { EventEmitter } = require("events");

/**
 * @typedef {Object} BrokerConfig
 *
 * @prop {String} [name="memory"] The name of the service that powers the broker,
 * currently supported brokers are: "rabbitmq" and "memory".
 * @prop {String} connectionString The connection string to connect to the broker
 * service, only needed for rabbitmq at present.
 */

/* eslint-disable class-methods-use-this, no-unused-vars */
/**
 * A broker is any service capable of receiving data to be persisted and then
 * relayed to a recipient worker to act upon it.
 * Any supported broker must comply with this interface.
 * @interface
 */
class Broker extends EventEmitter {
    /**
     * Enqueues a task in the Broker.
     *
     * @param {Task} task
     * @param {Number} [delay] The task will be enqueued in delay milliseconds.
     *
     * @return {Promise<Task>}
     */
    enqueue(task, delay) {
        return Promise.reject("Not implemented");
    }

    /**
     * Dequeues a task from the broker.
     *
     * @abstract
     * @param {String} name the name of the task
     *
     * @return {Promise<Task>}
     */
    dequeue(name) {
        return Promise.reject("Not implemented");
    }

    /**
     * Returns the number of tasks in the queue
     *
     * @abstract
     * @param {String} name The name of the task.
     *
     * @return {Promise<Number>}
     */
    tasksCount(name) {
        return Promise.reject("Not implemented");
    }

    /**
     * Finds a task with a given id inside the queue.
     * Not all brokers are capable of implementing this function
     *
     * @abstract
     * @param {String} name The name of the task.
     * @param {String} id The id of the task
     *
     * @return {Promise<Task>}
     */
    findTask(name) {
        return Promise.reject("Not implemented");
    }

    /**
     * Acknowledges the task as fulfilled.
     *
     * @abstract
     * @param {String} status The status of the task, either "failed" or
     * "success"
     * @param {Task} task The task being acknowledged
     * @param {*} result The result of the tasks
     */
    ack() {
    }
}

module.exports = Broker;
