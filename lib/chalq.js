/** @module {Chalq} */
const _ = require("lodash");

const MemoryBroker = require("./platforms/memory/broker");
const RabbitMQBroker = require("./platforms/rabbitmq/broker");

const Task = require("./task");
const Worker = require("./worker");

/**
 * The broker that as instantiated
 * @type {Broker}
 */
let broker = null;

/**
 * The tasks that have been registered in the Chalq instance
 * @type {Map<String, Proxy>}
 */
let tasks = new Map();

/**
 * The known states that a task may be in at a given time
 * @type {String[]}
 */
const KNOWN_STATES = ["failed", "success"];

const otherStates = _.memoize(state => _.without(KNOWN_STATES, state));

/**
 * The configuration object used to initialize the Chalq instance.
 * @typedef {Object} ChalqConfig
 *
 * @property {BrokerConfig} broker
 */

/**
 * The main Chalq class
 *
 * Chalq is exposed as a global object which is initialized by calling the
 * `initialze` method with the broker configuration.
 *
 * The exposed Chalq class is ultimately a Proxy class which provides means to
 * run tasks in a more concise way: `Chalq.my_task.run`, and also provides access
 * to the EventEmitter interface exposed by the broker.
 * @class
 */
class Chalq {
    /**
     * Initializes the Chalq instance by instantiating an internal broker
     * instance as per the passed
     * configuration
     *
     * This method should be called once.
     * @static
     * @param {ChalqConfig} config
     */
    static initialize(config = { broker: { name: "memory" } }) {
        if (broker) throw new Error("Chalq is already initialized");

        switch (config.broker.name) {
        case "memory":
            broker = new MemoryBroker();
            break;
        case "rabbitmq":
            broker = new RabbitMQBroker(config.broker.connectionString);
            break;
        default:
            throw new Error("Unknown broker type");
        }
    }

    /**
     * Registers a task in the Chalq instance.
     * Once a task is registered, it is proxied from the Chalq object by calling
     * `Chalq.<task_name>`
     *
     * @static
     * @param {String} name The name of the task
     * @param {TaskHandler} handler The handler of the task
     * @param {TaskOptions} options Default options applied to all new tasks.
     */
    static registerTask(name, handler, options) {
        if (!broker) throw new Error("Chalq has not been initialized");

        if (tasks.get(name)) {
            throw new Error("A task with this name has already been registered");
        }
        if (!_.isFunction(handler)) throw new Error("Handler is not a function");

        tasks.set(name, new Proxy({ name, handler }, {
            get(target, method) {
                if (method === "run") {
                    return _.partial(Chalq.runTask, target.name, _, _, options);
                } else if (method === "startWorker") {
                    return opts => new Worker(broker, name, handler, opts);
                } else if (method === "count") {
                    return _.bind(broker.tasksCount, broker, name);
                } else if (method === "find") {
                    return _.bind(broker.findTask, broker, name);
                }

                return undefined;
            }
        }));
    }

    /**
     * Runs a new task with the given parameters.
     * Instead calling this method directly, once you registered your task, you
     * can then run a new task by calling `Chalq.<task_name>.run(args)`
     *
     * @static
     * @private
     * @param {String} name The name of the task.
     * @param {Array<*>} args The arguments for the task
     * @param {TaskOptions} opts Options for the task
     * @param {TaskOptions} defaultOpts Default options of the task. This is
     * set by the proxy handler.
     *
     * @return {Promise<Task>}
     */
    static runTask(name, args, opts = {}, defaultOpts = {}) {
        if (!broker) {
            return Promise.reject(new Error("Chalq has not been initialized"));
        }

        if (!tasks.get(name)) {
            return Promise.reject(new Error(`Task '${name}' is not defined`));
        }

        _.defaults(opts, defaultOpts);

        const task = new Task(name, args, opts);

        return broker.enqueue(task).then((enqueuedTask) => {
            let handlers = new Map();

            handlers = ["failed", "success"].reduce((map, state) => {
                map.set(state, (result) => {
                    const others = otherStates(state);

                    others.forEach((s) => {
                        broker.removeListener(`${name}:${task.id}:${s}`,
                                              handlers.get(s));
                    });

                    task.emit(state, result);
                });

                return map;
            }, handlers);

            ["failed", "success"].forEach((state) => {
                broker.once(`${name}:${task.id}:${state}`, handlers.get(state));
            });

            return enqueuedTask;
        });
    }

    /**
     * Check if the Chalq instance is ready to receive tasks
     *
     * @static
     *
     * @return {Boolean}
     */
    static isReady() {
        return broker !== null;
    }

    /**
     * Destroys the Chalq instance. Once destroyed, Chalq can be initialized
     * again with a new configuration
     *
     * @static
     */
    static destroy() {
        broker = null;
        tasks = new Map();
    }
}

// Proxy event-related functions from Chalq to the broker
const eventMethods = ["on", "once", "listenerCount", "removeListener",
                      "removeAllListeners", "eventNames"];

eventMethods.forEach((method) => {
    Chalq[method] = (...args) => {
        if (!broker) throw new Error("Chalq has not been initialized");

        broker[method](...args);
    };
});

// Chalq is exposed as a Proxy object in order to allow for dynamic task name
// accessors.
const proxy = new Proxy(Chalq, {
    get(target, name) {
        return name in target ? target[name] : tasks.get(name);
    }
});

module.exports = proxy;
