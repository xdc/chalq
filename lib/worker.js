const _ = require("lodash");
const os = require("os");

/**
 * @typedef {Object} WorkerOptions
 *
 * @param {Number} concurrency
 */

/**
 * @typedef {Object} WorkerProcess
 *
 * @param {Function} stop
 * @param {Function} status
 */

/**
 * Wraps a task handler function in a timeout. If the timeout happens the task is
 * rejected but it doesn't mean that the handler itself was stopped, only that
 * the results will be ignored.
 *
 * @param {Function} func The partially applied task handler.
 * @param {Number} timeout
 *
 * @return {Promise<*>} Fulfilled with the result of the task handler if executed
 * on time
 */
function timeoutWrap(func, timeout) {
    return () => new Promise((resolve, reject) => {
        let fulfilled = false;
        const timeoutCallback = setTimeout(() => {
            if (!fulfilled) {
                reject("Task has timed out");
                fulfilled = true;
            }
        }, timeout);

        func().then((result) => {
            if (!fulfilled) {
                fulfilled = true;
                clearTimeout(timeoutCallback);
                resolve(result);
            }
        });
    });
}

class Worker {
    /**
     * @param {Broker} broker
     * @param {String} taskName
     * @param {TaskHandler}  handler
     * @param {WorkerOptions} opts
     */
    constructor(broker, taskName, handler, opts = {}) {
        if (!broker) throw new Error("Missing broker");
        if (!_.isString(taskName)) throw new Error("Missing task name");
        if (!_.isFunction(handler)) throw new Error("Missing handler");

        _.defaults(opts, {
            concurrency: os.cpus().length
        });

        /** @member {Broker} */
        this.broker = broker;
        /** @member {TaskHandler} */
        this.handler = handler;
        /** @member {String} */
        this.taskName = taskName;

        /** @member {WorkerProcess[]} */
        this._processes = _.times(opts.concurrency, () => this.startProcess());
    }

    /**
     * Begins a worker process
     *
     * @return {WorkerProcess}
     */
    startProcess() {
        let stop = false;

        const broker = this.broker;
        const handler = this.handler;
        const name = this.taskName;

        function runNext() {
            if (stop) return;

            broker.dequeue(name).then((task) => {
                if (!task) {
                    broker.once(`${name}:new`, runNext);
                    return;
                }

                let taskRun = _.partial(handler, ...task.args);

                if (task.opts.timeout) {
                    taskRun = timeoutWrap(taskRun, task.opts.timeout);
                }

                taskRun().then((result) => {
                    broker.ack("success", task, result);
                }).catch((err) => {
                    broker.ack("failed", task, err);
                }).then(() => {
                    runNext();
                });
            }).catch((err) => {
                console.log("Failed to dequeue next task, worker exiting.", err);
                stop = true;
                broker.removeListener(`${name}:task:new`, runNext);
            });
        }

        runNext();

        return {
            stop() {
                stop = true;
                broker.removeListener(`${name}:task:new`, runNext);
            },
            status() {
                return stop ? "stopped" : "started";
            }
        };
    }

    /**
     * Stops all processes of this worker
     */
    stop() {
        this._processes.forEach(p => p.stop());
    }
}

module.exports = Worker;
