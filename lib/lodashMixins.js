const _ = require("lodash");

/**
 * True if the passed number is a positive number
 *
 * @param {Number} number
 *
 * @return {Boolean}
 */
function isPositiveNumber(number) {
    return _.isFinite(number) && number > 0;
}

_.mixin({ isPositiveNumber }, { chain: false });
