require("chai").should();

const Task = require("../lib/task");

describe("Task", function () {
    describe("getDelay", function () {
        it("should return 0 if not specified", function () {
            const task = new Task("foo", [1, 2, 3], {
                retries: 4
            });

            const delay = Task.getDelay(task);
            delay.should.equal(0);
        });

        it("should return 0 if invalid delay property", function () {
            const task = new Task("foo", [1, 2, 3], {
                retries: 4,
                delay: "bogus"
            });

            const delay = Task.getDelay(task);
            delay.should.equal(0);
        });
        it("should default to 1000ms if negative delay number", function () {
            const task = new Task("foo", [1, 2, 3], {
                retries: 4,
                delay: -500
            });

            const delay = Task.getDelay(task);
            delay.should.equal(1000);
        });
        it("should default to linear backoff if not specified", function () {
            const task = new Task("foo", [1, 2, 3], {
                retries: 4,
                delay: {
                    amount: 4000
                }
            });

            const delay = Task.getDelay(task);
            delay.should.equal(4000 * 4);
        });
        it("should default to 1000ms if amount not specified", function () {
            const task = new Task("foo", [1, 2, 3], {
                retries: 4,
                delay: {
                }
            });

            const delay = Task.getDelay(task);
            delay.should.equal(1000 * 4);
        });
        it("should return correct delay for a linear backoff", function () {
            const task = new Task("foo", [1, 2, 3], {
                retries: 4,
                delay: {
                    backoff: "linear",
                    amount: 3000
                }
            });

            const delay = Task.getDelay(task);
            delay.should.equal(3000 * 4);
        });
        it("should return correct delay for an exponential backoff", function () {
            const task = new Task("foo", [1, 2, 3], {
                retries: 5,
                delay: {
                    backoff: "exponential",
                    amount: 2000
                }
            });

            const delay = Task.getDelay(task);
            delay.should.equal(Math.pow(2, 5 - 1) * 2000);
        });
        it("should return correct delay for a constant backoff", function () {
            const task = new Task("foo", [1, 2, 3], {
                retries: 5,
                delay: {
                    backoff: "constant",
                    amount: 2000
                }
            });

            const delay = Task.getDelay(task);
            delay.should.equal(2000);
        });
        it("should return correct delay for a specific delay", function () {
            const task = new Task("foo", [1, 2, 3], {
                retries: 5,
                delay: 2000
            });

            const delay = Task.getDelay(task);
            delay.should.equal(2000);
        });
    });
});
