const _ = require("lodash");
const Task = require("../../task");
const Broker = require("../../broker");
const { connect } = require("amqplib");

/** @var {Map<String, Message>} */
const MessageMap = new Map();


/**
 * Determines if retrieved object from queue is a message
 *
 * @param {Object} msg
 * @return {Boolean}
 */
function isMessage(msg) {
    return msg.content !== undefined;
}

/**
 * The RabbitMQ Broker expects that the rabbitmq server has the
 * `rabbitmq_delayed_message_exchange` plugin installed and enabled otherwise
 * the delay feature are not guaranteed to work correctly.
 */
class RabbitMQBroker extends Broker {
    /**
     * @param {String} cs RabbitMQ connection string
     */
    constructor(cs) {
        super();
        this.setMaxListeners(Infinity);
        connect(cs)
            .then(conn =>
                  Promise.all([conn.createChannel(), conn.createChannel()]))
            .then(([publish, consume]) => {
                /** @member {Channel} */
                this.publish = publish;
                /** @member {Channel} */
                this.consume = consume;
                /** @member {Map<String, Promise>} */
                this.queues = new Map();

                /** @member {Map<String, Promise} */
                this.queues = new Map();

                this.emit("ready");
            });
    }

    /**
     * @param {Task} task
     *
     * @return {Promise<Task>}
     */
    enqueue(task, delay) {
        return new Promise((resolve, reject) => {
            if (!this.publish) reject("Broker not initialized");

            const serialized = JSON.stringify(task.serialize());

            this.establishQueue(task.name)().then(() => {
                const ex = `${task.name}-exchange`;

                if (_.isPositiveNumber(delay)) {
                    return this.publish.publish(ex, "", new Buffer(serialized), {
                        headers: {
                            "x-delay": delay
                        }
                    });
                }

                return this.publish.sendToQueue(task.name,
                                                new Buffer(serialized),
                                                { persistent: true });
            }).then(() => {
                this.emit(`${task.name}:new`);
                resolve(task);
            }).catch(reject);
        });
    }

    /**
     * @param {String} name
     *
     * @return {Promise<Task>}
     */
    dequeue(name) {
        return this.publish.assertQueue(name)
            .then(() => this.consume.get(name, { noAck: false }))
            .then((message) => {
                if (!isMessage(message)) return null;

                const raw = message.content.toString();
                const serialized = JSON.parse(raw);
                const task = Task.unserialize(serialized);

                MessageMap.set(task.id, message);

                return task;
            });
    }

    /**
     * @param {String} name
     *
     * @return {Promise<Number>}
     */
    tasksCount(name) {
        return this.publish.assertQueue(name)
            .then(asserted => asserted.messageCount);
    }

    /* eslint-disable class-methods-use-this, no-unused-vars */
    /**
     * @param {String} name
     * @param {String} id
     *
     * @return {Promise<Task>}
     */
    findTask(name, id) {
        throw new Error("Finding a task with a RabbitMQ is not yet supported");
    }
    /* eslint-enable class-methods-use-this, no-unused-vars */

    /**
     * @param {String} status
     * @param {Task} task
     * @param {*} result
     */
    ack(status, task, result) {
        const message = MessageMap.get(task.id);

        this.consume.ack(message);
        this.emit(`${task.name}:${task.id}:${status}`, result);

        if (status === "failed" && task.opts.retries > 0) {
            task.opts.retries -= 1;

            this.enqueue(Task.unserialize(task));
        }
    }

    /**
     * Ensures a given queue is properly defined and asserted in the server
     * @private
     * @param {String}
     */
    establishQueue(name) {
        if (!this.queues.has(name)) {
            const assertQueue = _.bind(this._assertQueue, this, name);
            const assertExchange = _.bind(this._assertDelayExchange, this, name);
            const callback = _.once(() => assertQueue().then(assertExchange));

            this.queues.set(name, callback);
        }

        return this.queues.get(name);
    }

    /**
     * Asserts the queue existence for a given task name
     * @private
     *
     * @param {String} name
     *
     * @return {Promise}
     */
    _assertQueue(name) {
        return this.publish.assertQueue(name);
    }

    /**
     * Asserts the delay exchange existence for a given task name.
     * It also binds the corresponding queue to the exchange.
     * @private
     *
     * @param {String} name
     *
     * @return {Promise}
     */
    _assertDelayExchange(name) {
        const ex = `${name}-exchange`;

        return this.publish.assertExchange(ex, "x-delayed-message", {
            arguments: {
                "x-delayed-type": "direct"
            }
        }).then(() => this.publish.bindQueue(name, `${name}-exchange`));
    }
}

module.exports = RabbitMQBroker;
